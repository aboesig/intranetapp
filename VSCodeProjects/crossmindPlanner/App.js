import React from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, FlatList } from 'react-native';
import { DrawerNavigator } from 'react-navigation';
import { Button } from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class MyNavScreen extends React.Component {
  render() {
    return (
    <View style={{flex: 1, flexDirection: 'column'}}>
      <View style={styles.header}><MaterialIcons onPress={() => this.props.navigation.navigate('DrawerOpen')} name="menu" size={24} style={styles.menuIcon} /><Text style={styles.headerText}>Crossmind</Text></View>
        <ScrollView style={{width: '100%', flexGrow: 2, padding: 15}}>
        <View style={styles.categoryWrap}>
          <View style={styles.category1}><Text style={styles.categoryText}>Planning</Text></View>
          <View style={styles.category2}><Text style={styles.categoryText}>Ude</Text></View>
          <View style={styles.category3}><Text style={styles.categoryText}>Hjemme</Text></View>
        </View>
        <Text style={styles.title}>Calender</Text>
        <View style={styles.calContainer}>
        <FlatList
        data={[
          {key: 'Indledende møde', date: '24/7'},
          {key: 'Messe #1', date: '24/7'},
          {key: 'Tryk af plakat og flyers', date: '24/7'},
          {key: 'Brandguide', date: '24/7'},
          {key: 'Photoshoot', date: '24/7'},
          {key: 'Årligt møde', date: '24/7'},
          {key: 'Julefrokost', date: '24/7'},
          {key: 'Website redesign', date: '24/7'},
        ]}
        renderItem={({item}) => <View style={styles.ListitemWrap}><Text style={styles.Listitem}>{item.date} - {item.key}</Text></View>}/>
        </View>
      </ScrollView>
    </View>
    );
  }
}

const CalenderScreen = ({ navigation }) => (
  <MyNavScreen banner={'Calender Screen'} navigation={navigation} />
);
CalenderScreen.navigationOptions = {
  drawerLabel: 'Calender',
  drawerIcon: ({ tintColor }) => (
    <MaterialIcons
      name="date-range"
      size={24}
      style={{ color: tintColor }}
    />
  ),
};

const ProfileScreen = ({ navigation }) => (
  <MyNavScreen banner={'Profile Screen'} navigation={navigation} />
);
ProfileScreen.navigationOptions = {
  drawerLabel: 'Profile',
  drawerIcon: ({ tintColor }) => (
    <MaterialIcons 
    name="perm-identity" 
    size={24} style={{ color: tintColor }} />
  ),
};

const UpdateScreen = ({ navigation }) => (
  <MyNavScreen banner={'Update Screen'} navigation={navigation} />
);
UpdateScreen.navigationOptions = {
  drawerLabel: '#Updates',
  drawerIcon: ({ tintColor }) => (
    <MaterialIcons 
    name="view-agenda" 
    size={24} style={{ color: tintColor }} />
  ),
};

const DrawerExample = DrawerNavigator(
  {
    Calender: {
      path: '/',
      screen: CalenderScreen,
    },
    Updates: {
      path: '/updates',
      screen: UpdateScreen,
    },
    Profile: {
      path: '/profile',
      screen: ProfileScreen,
    },
  },
  {
    initialRouteName: 'Calender',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
  }
);

const styles = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  header: {
    height: 65,
    width: '100%',
    backgroundColor: '#363d56',
    justifyContent: 'center',
  },
  headerText: {
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 20,
  },
  menuIcon: {
    color: '#ffffff',
    top: 28,
    left: 15,
    zIndex: 1,
    width: 50,
  },
  title: {
    fontSize: 18,
  },
  calContainer: {
    padding: 20,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  ListitemWrap: {
    borderBottomWidth: 5,
    borderBottomColor: '#00A200',
    marginBottom: 15,
    width: '100%',
  },
  Listitem: {
    padding: 10,
  },
  categoryWrap: {
    flexDirection: 'row',
    marginBottom: 15,
    justifyContent: 'space-around',
  },
  category1: {
    height: 40,
    flexGrow: 1,
    backgroundColor: '#FFB100',
    padding: 5,
    justifyContent: 'center',
    marginRight: 5,
  },
  category2: {
    height: 40,
    flexGrow: 1,
    backgroundColor: '#00A200',
    padding: 5,
    justifyContent: 'center',
    marginRight: 5,
  },
  category3: {
    height: 40,
    flexGrow: 1,
    backgroundColor: '#BB0000',
    padding: 5,
    justifyContent: 'center',
  },
  categoryText: {
    color: '#ffffff',
    textAlign: 'center',
  },
});

export default DrawerExample;